package com.trutenko.finchtest.api

import com.trutenko.finchtest.data.User
import retrofit2.Call
import retrofit2.http.GET

interface GithubUserService {

    @GET("users")
    fun getUsers(): Call<Array<User>>

}