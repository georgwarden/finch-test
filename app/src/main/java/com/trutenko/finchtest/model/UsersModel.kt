package com.trutenko.finchtest.model

import com.trutenko.finchtest.api.GithubUserService
import com.trutenko.finchtest.data.User
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.async

class UsersModel(private val userService: GithubUserService) {

    private val cachedUsers: MutableList<User> = ArrayList(50)

    fun fetchUsers(): Deferred<List<User>> {
        return async {
            if (cachedUsers.isNotEmpty()) {
                cachedUsers
            } else {
                val response = userService.getUsers().execute()
                if (response.isSuccessful) {
                    response.body()!!.let { listOf(*it) }.also { cachedUsers.addAll(it) }
                } else {
                    throw Exception("${response.code()}:${response.message()}:${response.errorBody()?.string()}")
                }
            }
        }
    }

}