package com.trutenko.finchtest.view

import com.arellomobile.mvp.MvpView
import com.trutenko.finchtest.data.User

interface UsersListView: MvpView, LoadingView {

    fun growUserList(users: List<User>)

}