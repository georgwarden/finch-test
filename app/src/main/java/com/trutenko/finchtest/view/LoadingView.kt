package com.trutenko.finchtest.view

interface LoadingView {

    fun startLoading()
    fun stopLoading()

    fun showLoadingError()
    fun hideLoadingError()

}