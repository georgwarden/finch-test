package com.trutenko.finchtest

import android.app.Application
import com.squareup.moshi.Moshi
import com.trutenko.finchtest.api.GithubUserService
import com.trutenko.finchtest.model.UsersModel
import okhttp3.HttpUrl
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.net.URI

class App: Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        bind<GithubUserService>() with provider { provideUserService() }
        bind<UsersModel>() with singleton { UsersModel(instance()) }
    }

    private fun provideUserService(): GithubUserService {
        return Retrofit.Builder()
                .baseUrl(HttpUrl.get(URI.create("https://api.github.com/"))!!)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
                .create(GithubUserService::class.java)
    }

}