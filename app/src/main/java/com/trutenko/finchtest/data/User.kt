package com.trutenko.finchtest.data

import com.squareup.moshi.Json

data class User (
        @field:Json(name = "login") val nickname: String,
        @field:Json(name = "avatar_url") val avatarUrl: String,
        val reposAmount: Int = 0, // stub, because I suddenly need Rx
        @field:Json(name = "html_url") val directLink: String
)