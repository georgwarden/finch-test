package com.trutenko.finchtest.activity

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.trutenko.finchtest.GlideApp
import com.trutenko.finchtest.R

class UserActivity : AppCompatActivity() {

    companion object {

        const val PFP_LOCATION = "pfp_location"
        const val NAME = "name"
        const val REPOS_AMOUNT = "repos_amount"
        const val DIRECT_LINK = "direct_link"

        // omg Parcelable where
        fun newIntent(from: Context, pfpLocation: String, name: String, reposAmount: Int, directLink: String): Intent {
            val intent = Intent(from, UserActivity::class.java)
            intent.putExtra(PFP_LOCATION, pfpLocation)
            intent.putExtra(NAME, name)
            intent.putExtra(REPOS_AMOUNT, reposAmount)
            intent.putExtra(DIRECT_LINK, directLink)
            return intent
        }

    }

    lateinit var profilePicture: ImageView
    lateinit var name: TextView
    lateinit var publicReposAmount: TextView
    lateinit var directLinkButton: Button

    lateinit var directLink: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        profilePicture = findViewById(R.id.user_pfp)
        name = findViewById(R.id.user_name)
        publicReposAmount = findViewById(R.id.user_public_repos_amount)
        directLinkButton = findViewById(R.id.user_direct_link_button)

        intent.extras.also {
            GlideApp.with(this)
                    .load(it.getString(PFP_LOCATION))
                    .circleCrop()
                    .fitCenter()
                    .into(profilePicture)
            name.text = it.getString(NAME)
            publicReposAmount.text = it.getInt(REPOS_AMOUNT).toString()
            directLink =  it.getString(DIRECT_LINK)
        }

        directLinkButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(directLink)
            startActivity(intent)
        }
    }
}
