package com.trutenko.finchtest.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.trutenko.finchtest.view.UsersListView
import com.trutenko.finchtest.model.UsersModel
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch

@InjectViewState
class UsersListPresenter(private val usersModel: UsersModel): MvpPresenter<UsersListView>() {

    private var job: Job? = null

    init {
        requestUsersListGrowth()
    }

    private fun requestUsersListGrowth() {
        viewState.startLoading()
        job = launch(UI) {
            val users = usersModel.fetchUsers().await()
            viewState.growUserList(users)
            viewState.stopLoading()
        }.apply {
            invokeOnCompletion {
                job = null
                if (it != null) {
                    viewState.stopLoading()
                    viewState.startLoading()
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        job?.cancel()
    }

}