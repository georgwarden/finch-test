package com.trutenko.finchtest.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.trutenko.finchtest.GlideApp
import com.trutenko.finchtest.R
import com.trutenko.finchtest.data.User

class UsersAdapter(
        private val parentContext: Context,
        private val onItemPick: (User) -> Unit
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class UserViewHolder(root: View): RecyclerView.ViewHolder(root) {
        val pfp: ImageView = root.findViewById(R.id.user_pfp)
        val login: TextView = root.findViewById(R.id.user_login)
    }

    val users: MutableList<User> = ArrayList(50)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false).let { UserViewHolder(it) }

    override fun getItemCount() = users.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val userHolder = holder as UserViewHolder
        GlideApp.with(parentContext)
                .load(users[position].avatarUrl)
                .centerCrop()
                .fitCenter()
                .into(userHolder.pfp)
        userHolder.login.text = users[position].nickname
        userHolder.itemView.setOnClickListener { onItemPick(users[position]) }
    }

}